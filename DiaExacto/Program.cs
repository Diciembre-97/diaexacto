﻿using System;

namespace DiaExacto
{
    class Program
    {
        static void Main(string[] args)
        {
            string Date = DateTime.Now.ToString("dd-MM-yyyy");
            Console.WriteLine("Hoy estamos a {0}.", Date);
        }
    }
}
